

console.log(process.env.NODE_ENV);

const Path = require('path');
const Webpack = require('webpack');
const PostcssPresetEnv = require('postcss-preset-env');
const envs = ['develop', 'product']; // the last one should be product
const envType = process.env.NODE_ENV === 'product' ? 1 : 0; // change type here
const envName = envs[envType];
const gameFilePath = {
    develop: '.',
    // product: 'absolute/path/to/amazon/s3/file/server/rps'
    product: '.'
}[envName];
const isProduct = envName === envs[envs.length - 1];

const configs = [
    {
        name: 'game',
        entry: Path.resolve(__dirname, './dev/index.js'),
        output: {
            path: Path.resolve(__dirname, './dist/'),
            filename: 'game_bundle.js'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules)/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                presets: ['env']
                            }
                        }
                    ]
                },
                {
                    test: /\.css$/,
                    use: [
                        {
                            loader: 'style-loader', options: {
                                url: false // force loader not to parse url
                            }
                        },
                        {
                            loader: 'css-loader', options: {
                                url: false,
                                minimize: false
                            }
                        },
                    ]
                },
                {
                    test: /\.scss$/,
                    include: [
                        Path.resolve(__dirname, './dev/src'),
                    ],
                    use: [
                        {
                            loader: 'style-loader', options: {
                                url: false // force loader not to parse url
                            }
                        },
                        {
                            loader: 'css-loader', options: {
                                url: false,
                                minimize: false
                            }
                        },
                        {
                            loader: 'postcss-loader', options: { // use it's autoprefixer
                                ident: 'postcss',
                                plugins: (loader) => [
                                    PostcssPresetEnv()
                                ]
                            }
                        },
                        {
                            loader: 'sass-loader' // compiles Sass to CSS
                        }
                    ]
                },
                // // convert image file into base64
                // {
                //     // test: /\.(jpe?g|png|gif|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                //     test: /\.(jpe?g|png|gif?)(\?[a-z0-9=&.]+)?$/,
                //     use: [
                //         // {
                //         //     loader: 'raw-loader'
                //         // },
                //         {
                //             loader: 'url-loader',
                //             options: {
                //                 limit: 1000000000000000000
                //             }
                //         }
                //     ]
                // },
                // // convert font file into base64
                // {
                //     test: /\.(ttf|eot|woff|woff2)$/,
                //     loader: 'url-loader',
                //     options: {
                //         // Limit at 50k. Above that it emits separate files
                //         limit: 500000000000,
                //     },
                // },
            ]
        },
        plugins: []
    }
];

if (isProduct) {
    // uglyfy and remove logs, copy resorces
    configs.forEach((config) => {
        config.plugins.push(new Webpack.optimize.UglifyJsPlugin({
            // Compression specific options
            compress: {
                // show warnings
                warnings: false,
                // Drop console statements
                drop_console: true
            }
        }));
        config.resolve = {
            alias: {
                './image': Path.resolve(__dirname, './dev/src/image'),
                // 'audioFolder/': Path.resolve(__dirname, './dev/src/audio/'),
            }
        };
        config.module.rules.push(
            {
                test: /\.(jpe?g|png|gif|mp3|wav?)(\?[a-z0-9=&.]+)?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath: './',
                            name: (file) => {
                                return `${file.split('dev')[1]}`;
                            },
                        }
                    }
                ]
            }
        );
    });
} else { // not product
    // generate source-map
    configs.forEach((config) => {
        config.module.rules.push(
            {
                test: /\.(jpe?g|png|gif|mp3|wav?)(\?[a-z0-9=&.]+)?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            emitFile: false
                        }
                    }
                ]
            }
        );
        config.devtool = 'inline-source-map';
        config.devServer = {
            contentBase: Path.resolve(__dirname, './dev/src')
        };
        config.output.sourceMapFilename = `${config.name}_bundle.js.map`;
    });
}

module.exports = configs;