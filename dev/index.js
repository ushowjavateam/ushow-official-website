import webBasicStyle from './CSS/webBasicStyle.scss';
import shareStyle from './CSS/shareStyle.scss';
import deviceDesk from './CSS/deviceStyle_desk.scss';
import devicePhone from './CSS/deviceStyle_phone.scss';
import writing from './JS/writing';


function createDOM(tagName, idName, className) {
    var dom = document.createElement(tagName);
    if (idName) {
        dom.id = idName;
    }

    if (className) {
        dom.classList.add(className);
    }
    return dom;
}

function hambgerFunction() {
    var x = document.getElementById('hambgerSelect');
    if (!hambgerBtn) {
        hambgerBtn = true;
        x.style.transform = 'translateY(0vh)';
        x.style.transition= '0.6s ease';
    } else {
        hambgerBtn = false;
        x.style.transform = 'translateY(-80vh)';
        x.style.transition= '0.6s ease';
    }
}

function plusDivs(n) {
    if (!isMobile) {
        showDivs(slideIndex += n);
    } else {
        rwdShowDivs(slideRwdIndex += n)
    }
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("mySlides");
    if (n > x.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = x.length
    }
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";  
    }
    x[slideIndex-1].style.display = "inline-grid";
}

function rwdShowDivs(n) {
    var i;
    var x = document.getElementsByClassName("rwdSlides");
    console.log(x);
    if (n > x.length) {
        slideRwdIndex = 1
    }
    if (n < 1) {
        slideRwdIndex = x.length
    }
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";  
    }
    x[slideRwdIndex-1].style.display = "inline-grid";
}

var isMobile = false; //initiate as false
var hambgerBtn = false;
var slideIndex = 1;
var slideRwdIndex = 1;




// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

if (isMobile) {
    var desk = document.getElementsByClassName("mySlides");
    for (var i = 0; i < desk.length; i++) {
        desk[i].style.display = "none";
    }
    rwdShowDivs(slideRwdIndex);
} else {
    var phone = document.getElementsByClassName("rwdSlides");
    for (var i = 0; i < phone.length; i++) {
        phone[i].style.display = "none"; 
    }

    showDivs(slideIndex);
}
// console.log('Is phone?', isMobile);


document.getElementById('nav-hambger').addEventListener('click',function () {
    hambgerFunction();
});

var navHambger = document.getElementsByClassName('hambgerMenu');
for (var i = 0; i < navHambger.length; i++) {
    navHambger[i].addEventListener('click',function () {
        hambgerFunction();
    });
}


var homeRight = document.getElementById('homeRight');
homeRight.appendChild(createDOM('a',)).innerHTML = `<span>${writing.pictureDiscrip[0].title}</span>
<br><br><span>${writing.pictureDiscrip[0].content}</span><br>`;
homeRight.appendChild(createDOM('a',)).innerHTML = `<span>${writing.pictureDiscrip[1].title}</span>
<br><br><span>${writing.pictureDiscrip[1].content}</span><br>`;
homeRight.appendChild(createDOM('a',)).innerHTML = `<span>${writing.pictureDiscrip[2].title}</span>
<br><br><span>${writing.pictureDiscrip[2].content}</span><br>`;


document.getElementById('prev').addEventListener('click', function () {
    plusDivs(-1);
});
document.getElementById('next').addEventListener('click', function () {
    plusDivs(1);
});


var newsTitle = document.getElementById('newsTitle');
newsTitle.innerHTML = '最新消息';
var newsContext = document.getElementById('newsContext');
newsContext.innerHTML = writing.latestNews;


var recruitVender = document.getElementById('recruitVender');
recruitVender.appendChild(createDOM('a')).innerHTML = `${writing.recruitVender[0].text} &nbsp; 
<span>${writing.recruitVender[1].text}</span> <br>
<span>${writing.recruitVender[2].text}</span>`


var about = document.getElementById('about');
about.appendChild(createDOM('a')).innerHTML = writing.about[0].title + '<br>';
about.appendChild(createDOM('a')).innerHTML = writing.about[1].textZh + '<br>';
about.appendChild(createDOM('a')).innerHTML = writing.about[1].textUs + '<br><br>';
about.appendChild(createDOM('a')).innerHTML = writing.about[2].textZh + '<br>';
about.appendChild(createDOM('a')).innerHTML = writing.about[2].textUs;


var contact = document.getElementById('contact');
if (isMobile) {
    contact.appendChild(createDOM('a')).innerHTML = writing.contact[0].title + '<br>';
    contact.appendChild(createDOM('a')).innerHTML = writing.contact[1].text + '<br>';
    contact.appendChild(createDOM('a')).innerHTML = writing.contact[2].phone +'<br>';
    contact.appendChild(createDOM('a')).innerHTML = writing.contact[2].mail +'<br>';

} else{
    contact.appendChild(createDOM('a')).innerHTML = writing.contact[0].title + '<br>';
    contact.appendChild(createDOM('a')).innerHTML = writing.contact[1].text + '<br>';
    contact.appendChild(createDOM('a')).innerHTML = writing.contact[2].text;
}

var footer = document.getElementById('footer');
footer.innerHTML = `${writing.footer[0].text} &nbsp; 
<span>${writing.footer[1].text}</span> <br> 
<span>${writing.footer[2].text}</span>`;





