

console.log(process.env.NODE_ENV);

const Path = require('path');
const Webpack = require('webpack');
const PostcssPresetEnv = require('postcss-preset-env');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const configs = [
    {
        mode: 'production',
        name: 'ushow_website',
        entry: Path.resolve(__dirname, '../dev/index.js'),
        output: {
            path: Path.resolve(__dirname, '../dist/'),
            filename: 'index.js',
            sourceMapFilename: `index.js.map`
        },
        module: {
            rules: [
                {
                    test: /\.vue$/,
                    loader: 'vue-loader'
                },
                {
                    test: /\.js$/,
                    exclude: /(node_modules)/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                presets: ['@babel/preset-env']
                            }
                        }
                    ]
                },
                {
                    test: /\.scss$/,
                    include: [
                        Path.resolve(__dirname, '../dev/CSS'),
                        Path.resolve(__dirname, '../dev/vue'),
                    ],
                    use: [
                        {
                            loader: 'style-loader', options: {

                            }
                        },
                        {
                            loader: 'css-loader', options: {
                                url: false,
                                minimize: false
                            }
                        },
                        {
                            loader: 'postcss-loader', options: { // use it's autoprefixer
                                ident: 'postcss',
                                plugins: (loader) => [
                                    PostcssPresetEnv()
                                ]
                            }
                        },
                        {
                            loader: 'sass-loader' // compiles Sass to CSS
                        }
                    ]
                },
            ]
        },
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.esm.js' // 用 webpack 1 时需用 'vue/dist/vue.common.js'
            }
        },
        // devtool: 'inline-source-map',
        plugins: [
            // 请确保引入这个插件！
            new VueLoaderPlugin()
        ]

    },

];


// // generate source-map
// configs.forEach((config) => {
//     config.module.rules.push(
//         {
//             test: /\.(jpe?g|png|gif|mp3|wav?)(\?[a-z0-9=&.]+)?$/,
//             exclude: /node_modules/,
//             use: [
//                 {
//                     loader: 'file-loader',
//                     options: {
//                         emitFile: false
//                     }
//                 }
//             ]
//         }
//     );

//     config.devServer = {
//         contentBase: Path.resolve(__dirname, './dev/src')
//     };
//     config.output.sourceMapFilename = `${config.name}_bundle.js.map`;
// });


module.exports = configs;