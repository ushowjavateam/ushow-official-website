let path = require('path');
let express = require('express');
let app = express();
let http = require('http').Server(app);
var io = require('socket.io')(http);

//使用./dist和./dev路徑下的東西
app.use('/', express.static(path.resolve(__dirname, './dist')));
app.use('/', express.static(path.resolve(__dirname, './dev')));

app.set('view engine', 'ejs');
app.set('views', path.resolve(__dirname, './view'));

// route

app.get('/', function (req, res) {
    res.render('ushow_website');
});

let port = 1011;

http.listen(port, function () {
    console.log(`App server now listening on port ${port}`);
});